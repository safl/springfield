#ifndef COMMON_H
#define COMMON_H

#include <iostream>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <math.h>

#include <time.h>

using namespace std;

#ifdef __APPLE__
#include "OpenCL/opencl.h"
#else
#include "CL/cl.h"
#endif

#define OPCODE_MIN      0
#define OPCODE_MAX      1

#define CPHVB_INT8      0
#define CPHVB_INT16     1
#define CPHVB_INT32     2
#define CPHVB_INT64     3
#define CPHVB_UINT8     4
#define CPHVB_UINT16    5
#define CPHVB_UINT32    6
#define CPHVB_UINT64    7
#define CPHVB_FLOAT32   8
#define CPHVB_FLOAT64   9

#define BITS_PER_1_BYTE 8
#define BITS_PER_2_BYTE 16
#define BITS_PER_4_BYTE 32
#define BITS_PER_8_BYTE 64

#define RADIX           8
#define RADIX_MASK      0xFF
#define BUCKET_SIZE     256

#define WORKGROUPS_PER_CU 4

#define WORKITMES_PER_GROUP_RADIXSELECT     128
#define WORKITMES_PER_GROUP_FILTER          128
#define WORKITMES_PER_GROUP_PREFIXSUM       128

#define NUM_KVALUE_OCL_MAX         1024

#define CL_STRING_LENGTH 128

#define PIPELINE 0
#define STAGES 5

#define DEBUGGING_PRINT  0
#define TIMER 1

#define CPU_FILTER 0

#endif // COMMON_H

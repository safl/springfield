#ifndef BASICCL_H
#define BASICCL_H

#include "common.h"

class BasicCL
{
public:
    BasicCL();

    int getNumPlatform(unsigned int *numPlatforms);
    int getPlatformIDs(cl_platform_id *platforms, unsigned int numPlatforms);
    int getPlatformInfo(cl_platform_id platform, char *platformVendor, char *platformVersion);

    int getNumCpuDevices(cl_platform_id platform, unsigned int *numCpuDevices);
    int getNumGpuDevices(cl_platform_id platform, unsigned int *numGpuDevices);
    int getCpuDeviceIDs(cl_platform_id platform, unsigned int numCpuDevices, cl_device_id *cpuDevices);
    int getGpuDeviceIDs(cl_platform_id platform, unsigned int numGpuDevices, cl_device_id *gpuDevices);
    int getDeviceInfo(cl_device_id device, char *deviceName, char *deviceVersion,
                      unsigned int *deviceComputeUnits, unsigned int *deviceGlobalMem, 
                      unsigned int *deviceLocalMem, unsigned int *maxSubDevices);

    int getContext(cl_context *context, cl_device_id *devices, unsigned int numDevices);

    int getCommandQueue(cl_command_queue *commandQueue, cl_context context, cl_device_id device);

    int getProgram(cl_program *program, cl_context context, const char *kernelSourceCode);
    int getKernel(cl_kernel *kernel, cl_program program, const char *kernelName);

private:
    cl_int  _ciErr;
};

#endif // BASICCL_H

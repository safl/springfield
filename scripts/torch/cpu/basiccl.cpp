#include "basiccl.h"

BasicCL::BasicCL()
{
}

int BasicCL::getNumPlatform(unsigned int *numPlatforms)
{
    _ciErr = clGetPlatformIDs(0, NULL, numPlatforms);
    return _ciErr;
}

int BasicCL::getPlatformIDs(cl_platform_id *platforms, unsigned int numPlatforms)
{
    _ciErr = clGetPlatformIDs(numPlatforms, platforms, NULL);
    return _ciErr;
}

int BasicCL::getPlatformInfo(cl_platform_id platform, char *platformVendor, char *platformVersion)
{
    _ciErr = clGetPlatformInfo(platform, CL_PLATFORM_VENDOR, CL_STRING_LENGTH*sizeof(char), platformVendor, NULL);
    if(_ciErr != CL_SUCCESS) return _ciErr;
    _ciErr = clGetPlatformInfo(platform, CL_PLATFORM_VERSION, CL_STRING_LENGTH*sizeof(char), platformVersion, NULL);
    if(_ciErr != CL_SUCCESS) return _ciErr;
    return CL_SUCCESS;
}

int BasicCL::getNumCpuDevices(cl_platform_id platform, unsigned int *numCpuDevices)
{
    _ciErr = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 0, NULL, numCpuDevices);
    return _ciErr;
}

int BasicCL::getCpuDeviceIDs(cl_platform_id platform, unsigned int numCpuDevices, cl_device_id *CpuDevices)
{
    _ciErr = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, numCpuDevices, CpuDevices, NULL);
    return _ciErr;
}

int BasicCL::getNumGpuDevices(cl_platform_id platform, unsigned int *numGpuDevices)
{
    _ciErr = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 0, NULL, numGpuDevices);
    return _ciErr;
}

int BasicCL::getGpuDeviceIDs(cl_platform_id platform, unsigned int numGpuDevices, cl_device_id *GpuDevices)
{
    _ciErr = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, numGpuDevices, GpuDevices, NULL);
    return _ciErr;
}

int BasicCL::getDeviceInfo(cl_device_id device, char *deviceName, char *deviceVersion,
                           unsigned int *deviceComputeUnits, unsigned int *deviceGlobalMem, 
                           unsigned int *deviceLocalMem, unsigned int *maxSubDevices)
{
    _ciErr = clGetDeviceInfo(device, CL_DEVICE_NAME, CL_STRING_LENGTH*sizeof(char), deviceName, NULL);
    if(_ciErr != CL_SUCCESS) return _ciErr;
    _ciErr = clGetDeviceInfo(device, CL_DEVICE_VERSION, CL_STRING_LENGTH*sizeof(char), deviceVersion, NULL);
    if(_ciErr != CL_SUCCESS) return _ciErr;
    _ciErr = clGetDeviceInfo(device, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(cl_uint), deviceComputeUnits, NULL);
    if(_ciErr != CL_SUCCESS) return _ciErr;
    _ciErr = clGetDeviceInfo(device, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(cl_ulong), deviceGlobalMem, NULL);
    if(_ciErr != CL_SUCCESS) return _ciErr;
    _ciErr = clGetDeviceInfo(device, CL_DEVICE_LOCAL_MEM_SIZE, sizeof(cl_ulong), deviceLocalMem, NULL);
    if(_ciErr != CL_SUCCESS) return _ciErr;
    _ciErr = clGetDeviceInfo(device, CL_DEVICE_PARTITION_MAX_SUB_DEVICES, sizeof(cl_uint), maxSubDevices, NULL);
    if(_ciErr != CL_SUCCESS) return _ciErr;    
    return CL_SUCCESS;
}

int BasicCL::getContext(cl_context *context, cl_device_id *devices, unsigned int numDevices)
{
    context[0] = clCreateContext(0, numDevices, devices, NULL, NULL, &_ciErr);
    //cout << "------------ _ciErr " << _ciErr << endl;
    return _ciErr;
}

int BasicCL::getCommandQueue(cl_command_queue *commandQueue, cl_context context, cl_device_id device)
{
    commandQueue[0] = clCreateCommandQueue(context, device, CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE, &_ciErr);
    //CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE
    //CL_QUEUE_PROFILING_ENABLE
    //cout << "------------ _ciErr " << _ciErr << endl;
    return _ciErr;
}

int BasicCL::getProgram(cl_program *program, cl_context context, const char *kernelSourceCode)
{
    size_t SourceSize[] = { strlen(kernelSourceCode)};
    program[0] = clCreateProgramWithSource(context, 1, &kernelSourceCode, SourceSize, &_ciErr); //cout << "------------ _ciErr " << _ciErr << endl;
    if(_ciErr != CL_SUCCESS) return _ciErr;
    _ciErr = clBuildProgram(program[0], 0, NULL, NULL, NULL, NULL); //cout << "------------ _ciErr " << _ciErr << endl;
    if(_ciErr != CL_SUCCESS) return _ciErr;
    return CL_SUCCESS;
}

int BasicCL::getKernel(cl_kernel *kernel, cl_program program, const char *kernelName)
{
    kernel[0] = clCreateKernel(program, kernelName, &_ciErr);
    return _ciErr;
}

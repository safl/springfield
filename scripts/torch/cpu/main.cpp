#include <iostream>
#include "pthread.h"
#include "basiccl.h"

#define ITEMS_PER_WORKGROUP 256
#define WORKGROUPS_PER_THREAD 65536

using namespace std;

BasicCL basicCL;

const char *kernelSourceCode =
    "__kernel void runCL(uint loop, __global float *array, __local float8 *localMem)\n"
    "{\n"
    "    uint local_id_x  = get_local_id(0); \n"
    "    uint global_id_x = get_global_id(0); \n"
    "    float x = array[global_id_x]; \n"
    "    float8 a = (float8)(1.0f);\n"
    "    float8 b = (float8)(2.0f);\n"
    "    float8 c = (float8)(0.5f);\n"
    "    float8 r = (float8)(0.0f);\n"
    "    localMem[local_id_x] = r;\n"
    "    for (int i = 0; i < loop; i++) \n"
    "    { \n"
    "        localMem[local_id_x] = asinh(c) + acosh(a) * asinh(b);\n"
    "        a = asinh(c) + acosh(localMem[local_id_x]) * asinh(b);\n"
    "        localMem[local_id_x] = asinh(c) + acosh(a) * asinh(r);\n"
    "        b = asinh(r) + acosh(a) * asinh(localMem[local_id_x]);\n"
    "        localMem[local_id_x] = asinh(c) + acosh(r) * asinh(b);\n"
    "    } \n"
    "    array[global_id_x] = x * x; \n"
    "} \n";
            
// basic OpenCL variables
char _platformVendor[CL_STRING_LENGTH];
char _platformVersion[CL_STRING_LENGTH];

char _deviceName[CL_STRING_LENGTH];
char _deviceVersion[CL_STRING_LENGTH];
unsigned int _deviceComputeUnits;
unsigned int _deviceGlobalMem;
unsigned int _deviceLocalMem;
unsigned int _deviceMaxSubDevices; 

cl_uint             _numPlatforms;          // OpenCL platform
cl_platform_id*     _cpPlatforms;
cl_uint             _numCpuDevices;         // OpenCL CPU device
cl_device_id*       _cdCpuDevices;
cl_uint             _numGpuDevices;         // OpenCL GPU device
cl_device_id*       _cdGpuDevices;
cl_context          _cxCpuContext;          // OpenCL CPU context
cl_context          _cxGpuContext;          // OpenCL GPU context

cl_program          _cpCpu;      // OpenCL CPU program
cl_program          _cpGpu;      // OpenCL GPU program
cl_kernel           _ckCpu;      // OpenCL CPU kernel
cl_kernel           _ckGpu;      // OpenCL GPU kernel

cl_command_queue*   _cqCommandQueue;
cl_mem* d_array;

size_t szLocalWorkSize[1];
size_t szGlobalWorkSize[1];

unsigned int loop = 1000;

void *threadExe(void *threadid)
{
    long tid;
    tid = (long)threadid;
    cout << "Thread " << tid << " is working." << endl;
    
    cl_kernel local_kernel;
    
    int err = 0;
    if (tid < _numCpuDevices)
        local_kernel = _ckCpu;
    else
        local_kernel = _ckGpu;
        
    err = clSetKernelArg(local_kernel, 0, sizeof(cl_uint), (void*)&loop);
    err |= clSetKernelArg(local_kernel, 1, sizeof(cl_mem), (void*)&d_array[tid]);
    err |= clSetKernelArg(local_kernel, 2, sizeof(cl_float8) * ITEMS_PER_WORKGROUP, NULL);
    if(err != CL_SUCCESS) {
        cout << "arg error = " << err << endl;
    }

    if (tid == 0) {
        for(int i=0; i<100000;i++)
            err = clEnqueueNDRangeKernel(_cqCommandQueue[tid], local_kernel, 1,
                                 NULL, szGlobalWorkSize, szLocalWorkSize, 0, NULL, NULL);
    }

    if(err != CL_SUCCESS) {
        cout << "run error = " << err << endl;
    }
        
    pthread_exit((void *)threadid);
}

int main()
{    
    // init OpenCL
    int err = 0;
    szLocalWorkSize[0]  = ITEMS_PER_WORKGROUP;
    szGlobalWorkSize[0] = WORKGROUPS_PER_THREAD * szLocalWorkSize[0];
                    
    // platform
    err = basicCL.getNumPlatform(&_numPlatforms);
    if(err != CL_SUCCESS) { cout << "platform error = " << err << endl; /*return err;*/ }
    cout << endl << "platform number: " << _numPlatforms << endl;

    _cpPlatforms = (cl_platform_id *)malloc(sizeof(cl_platform_id) * _numPlatforms);

    err = basicCL.getPlatformIDs(_cpPlatforms, _numPlatforms);
    if(err != CL_SUCCESS) { cout << "platform error = " << err << endl; /*return err;*/ }

    for (unsigned int i = 0; i < _numPlatforms; i++)
    {
        err = basicCL.getPlatformInfo(_cpPlatforms[i], _platformVendor, _platformVersion);
        if(err != CL_SUCCESS) { cout << "platform error = " << err << endl; /*return err;*/ }
        cout << "Platform [" << i <<  "] Vendor: " << _platformVendor << ", Version: " << _platformVersion << endl;
    }
    
    // Cpu device
    err = basicCL.getNumCpuDevices(_cpPlatforms[0], &_numCpuDevices);
    _cdCpuDevices = (cl_device_id *)malloc(_numCpuDevices * sizeof(cl_device_id) );
    err = basicCL.getCpuDeviceIDs(_cpPlatforms[0], _numCpuDevices, _cdCpuDevices);
    if(err != CL_SUCCESS) { cout << "device error = " << err << endl; /*return err;*/ }
    cout << endl << _numCpuDevices << " Cpu device(s)" << endl;
    for (unsigned int i = 0; i < _numCpuDevices; i++)
    {
        err = basicCL.getDeviceInfo(_cdCpuDevices[i], _deviceName, _deviceVersion,
                                     &_deviceComputeUnits, &_deviceGlobalMem, 
                                     &_deviceLocalMem, &_deviceMaxSubDevices);
        if(err != CL_SUCCESS) { cout << "device error = " << err << endl; /*return err;*/ }
        cout << "Cpu device[" << i << "]: "
             << _deviceName << endl << " ("
             << _deviceComputeUnits << " compute units, "
             << _deviceLocalMem/1024 << " KB local, "
             << _deviceGlobalMem/1024/1024 << " MB global, "
             << _deviceMaxSubDevices << " sub-devices, "
             << _deviceVersion << ")" << endl;
    }

    // Gpu device
    err = basicCL.getNumGpuDevices(_cpPlatforms[0], &_numGpuDevices);
    _cdGpuDevices = (cl_device_id *)malloc(_numGpuDevices * sizeof(cl_device_id) );
    err |= basicCL.getGpuDeviceIDs(_cpPlatforms[0], _numGpuDevices, _cdGpuDevices);
    if(err != CL_SUCCESS) { cout << "device error = " << err << endl; /*return err;*/ }
    cout << endl << _numGpuDevices << " Gpu device(s)" << endl;
    for (unsigned int i = 0; i < _numGpuDevices; i++)
    {
        err |= basicCL.getDeviceInfo(_cdGpuDevices[i], _deviceName, _deviceVersion,
                                     &_deviceComputeUnits, &_deviceGlobalMem, 
                                     &_deviceLocalMem, &_deviceMaxSubDevices);
        if(err != CL_SUCCESS) { cout << "device error = " << err << endl; /*return err;*/ }
        cout << "Gpu device[" << i << "]: "
             << _deviceName << endl << " ("
             << _deviceComputeUnits << " compute units, "
             << _deviceLocalMem/1024 << " KB local, "
             << _deviceGlobalMem/1024/1024 << " MB global, "
             << _deviceMaxSubDevices << " sub-devices, "
             << _deviceVersion << ")" << endl;
    }

    _cqCommandQueue = (cl_command_queue*)malloc((_numCpuDevices + _numGpuDevices) * sizeof(cl_command_queue));

    // Cpu & Gpu context
    err = basicCL.getContext(&_cxCpuContext, _cdCpuDevices, _numCpuDevices);
    if(err != CL_SUCCESS) { cout << "cpu context error = " << err << endl; /*return err;*/ }
    err = basicCL.getContext(&_cxGpuContext, _cdGpuDevices, _numGpuDevices);
    if(err != CL_SUCCESS) { cout << "gpu context error = " << err << endl; /*return err;*/ }
    
    // program & kernel on Cpu context
    const char *kernelNamePthreadOCL = "runCL";
    
    err  = basicCL.getProgram(&_cpCpu, _cxCpuContext, kernelSourceCode); 
    err |= basicCL.getKernel(&_ckCpu, _cpCpu, kernelNamePthreadOCL); 
    if(err != CL_SUCCESS) { cout << "kernel error = " << err << endl; /*return err;*/ }

    err  = basicCL.getProgram(&_cpGpu, _cxGpuContext, kernelSourceCode); 
    err |= basicCL.getKernel(&_ckGpu, _cpGpu, kernelNamePthreadOCL); 
    if(err != CL_SUCCESS) { cout << "kernel error = " << err << endl; /*return err;*/ }
    
    // Cpu & Gpu commandqueue
    for (unsigned int i = 0; i < _numCpuDevices; i ++)
    {
        err = basicCL.getCommandQueue(&_cqCommandQueue[i], _cxCpuContext, _cdCpuDevices[i]);
        if(err != CL_SUCCESS) { cout << "cq error = " << err << endl;}
    }
    for (unsigned int i = _numCpuDevices; i < _numGpuDevices + _numCpuDevices; i ++)
    {
        err = basicCL.getCommandQueue(&_cqCommandQueue[i], _cxGpuContext, _cdGpuDevices[i-_numCpuDevices]);
        if(err != CL_SUCCESS) { cout << "cq error = " << err << endl;}
    }
    
    // array    
    float *h_array = (float *)malloc(sizeof(float) * ITEMS_PER_WORKGROUP * WORKGROUPS_PER_THREAD);
    memset(h_array, 0, sizeof(float) * ITEMS_PER_WORKGROUP * WORKGROUPS_PER_THREAD);
    d_array = (cl_mem*)malloc(sizeof(cl_mem) * (_numGpuDevices + _numCpuDevices));
    for (unsigned int i = 0; i < _numCpuDevices; i ++)
    {
        d_array[i] = clCreateBuffer(_cxCpuContext, CL_MEM_READ_WRITE, 
                                    sizeof(float) * ITEMS_PER_WORKGROUP * WORKGROUPS_PER_THREAD, NULL, &err);
        if(err != CL_SUCCESS) { cout << "mem error = " << err << endl;}
        err = clEnqueueWriteBuffer(_cqCommandQueue[i],
                               d_array[i], CL_TRUE, 0, 
                               sizeof(float) * WORKGROUPS_PER_THREAD * ITEMS_PER_WORKGROUP,
                               h_array, 0, NULL, NULL);
        if(err != CL_SUCCESS) { cout << "h2d error = " << err << endl; /*return err;*/ }
    }
    for (unsigned int i = _numCpuDevices; i < _numGpuDevices + _numCpuDevices; i ++)
    {
        d_array[i] = clCreateBuffer(_cxGpuContext, CL_MEM_READ_WRITE, 
                                    sizeof(float) * ITEMS_PER_WORKGROUP * WORKGROUPS_PER_THREAD, NULL, &err);
        if(err != CL_SUCCESS) { cout << "mem error = " << err << endl;}
        err = clEnqueueWriteBuffer(_cqCommandQueue[i],
                               d_array[i], CL_TRUE, 0, 
                               sizeof(float) * WORKGROUPS_PER_THREAD * ITEMS_PER_WORKGROUP,
                               h_array, 0, NULL, NULL);
        if(err != CL_SUCCESS) { cout << "h2d error = " << err << endl; /*return err;*/ }
    }
    
    // Pthread part
    pthread_t *threads = (pthread_t *)malloc(sizeof(pthread_t) * (_numCpuDevices + _numGpuDevices));
    pthread_attr_t attr;
    int rc;
    long t;
    void *status;
    
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
        
    for (t = 0; t < _numCpuDevices + _numGpuDevices; t++)
    {
        rc = pthread_create(&threads[t], &attr, threadExe, (void *)t);
        if (rc)
        {
            cout << "errors here!" << endl;
            exit(-1);
        }
    }

    pthread_attr_destroy(&attr);
    
    for (t = 0; t < _numCpuDevices + _numGpuDevices; t++)
    {
        rc = pthread_join(threads[t], &status);
        if (rc)
        {
            cout << "errors here!" << endl;
            exit(-1);
        }
        //cout << "thread[" << t << "] status is " << (long)status << endl;
    }
    
    if(threads) free(threads);
    if(_cqCommandQueue) free(_cqCommandQueue);
    
    err = clReleaseKernel(_ckCpu);
    err |= clReleaseProgram(_cpCpu);
    err |= clReleaseContext(_cxCpuContext);
    err |= clReleaseKernel(_ckGpu);
    err |= clReleaseProgram(_cpGpu);
    err |= clReleaseContext(_cxGpuContext);
    if(err != CL_SUCCESS) { cout << "free error = " << err << endl; }
}


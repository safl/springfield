#!/usr/bin/env python
from subprocess import Popen, PIPE, call
import curses
import sys
import os
import time
import re

def start( displays ):
    """Execute the 'burner' programs."""

    burn_gpu = ['./gputest', '/test=fur', '/width=1920', '/height=1080', '/fullscreen']
    burn_cpu = ['./cputest']

    pids = []
    for display in displays:            # Start GPU burners
        e = os.environ.copy()
        e['DISPLAY'] = display
        pids.append( Popen(burn_gpu, env=e).pid )

    pids.append( Popen(burn_cpu).pid )  # Start CPU burner

    return pids

def stop( pids=[] ):
    """Terminate the 'burner' programs."""

    for pid in pids:
        call(["kill", "%d" % pid])

    call(['pkill', '-f', 'gputest'])
    call(['pkill', '-f', 'cputest'])

def sample():
    """Sample sensor-info from 'sensors' and aticonfig."""

    cpu_regex = "((?:temp\d+)|(?:power\d+)):\s+\+?(\d+\.\d+)"
    gpu_regex = "Adapter\s(\d+)\s-\s(.*)\n\s+Sensor 0: Temperature\s-\s([0-9.]+)\sC"

    cpu_p = Popen(['sensors'], stdout=PIPE)
    gpu_p = Popen(['aticonfig', '--odgt', '--adapter=all'], stdout=PIPE)

    cpu_out, cpu_err = cpu_p.communicate()
    gpu_out, gpu_err = gpu_p.communicate()

    cpu = []
    gpu = []

    for m in re.finditer(cpu_regex, cpu_out):
        cpu.append( m.groups() )

    for m in re.finditer(gpu_regex, gpu_out):
        gpu.append( m.groups() )

    return (cpu, gpu)

def nop():
    pass

def main():
    
    menu = [
        ('T', 'Toggle', nop),
        ('Q', 'Quit',   nop),
    ]
    ver  = "0.1"
    burning = False
    stop()

    displays = [":0.1", ":0.2", ":0.0"]

    text = {
        'title':    "--{ Burner v%s }--",
        'elapsed':  "Elapsed: %dmin %dsec"
    }

    s = curses.initscr()
    delay = 1000

    stamp = time.time()
    cmd = 't'
    while cmd != 'q':

        if cmd == 't':
            stamp = time.time()
            if burning:
                stop()
                burning = False
            else:
                start( displays )
                burning = True

        mins    = (time.time() - stamp)/60
        secs    = (time.time() - stamp)%60

        cpu_samples, gpu_samples = sample()

        s.clear()
        s.border(0)
        s.timeout(delay)
        y,x = s.getmaxyx()
        s.hline(2, 1, curses.ACS_HLINE, x-2)

        txt = text['title'] % (ver)
        s.addstr(1,int(x/2)-int((len(txt)/2)),txt)

        txt = text['elapsed'] % (mins, secs)
        s.addstr(y-2, 2, txt)

        samples = [['MB Sensors']] + cpu_samples + ['', ['GPU Sensors']] + gpu_samples

        for c, sensor in enumerate(samples):
            s.addstr(3+c,2, "%s" % ' - '.join(sensor))
        s.refresh()

        try:
            cmd = s.getkey().lower()                # Get user input
        except Exception:
            cmd = ''

    curses.endwin()
    stop()

if __name__ == "__main__":
    main()


GpuTest 0.2 Linux 64-bit
------------------------

This is re-distribution of the FurMark based linux-version of GpuTest from http://www.geeks3d.com/20121113/gputest-0-2-0-cross-platform-opengl-benchmark-furmark-lands-on-linux-and-os-x.
This re-distribution has a couple of name-changes and removed some unneeded files.
Here is how to use it::

  ./gputest /test=fur /width=1920 /height=1080 /fullscreen /benchmark
  ./gputest /test=fur /width=1024 /height=640
  ./gputest /test=gi /width=1920 /height=1080 /fullscreen /benchmark
  ./gputest /test=gi /width=1024 /height=640
  ./gputest /test=tess /width=1920 /height=1080 /fullscreen /benchmark
  ./gputest /test=tess /width=1024 /height=640



DISPLAY=:0.0 ./gputest /test=fur /width=1920 /height=1080 /fullscreen &
DISPLAY=:0.1 ./gputest /test=fur /width=1920 /height=1080 /fullscreen &
DISPLAY=:0.2 ./gputest /test=fur /width=1920 /height=1080 /fullscreen &

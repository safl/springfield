========
Welcome!
========

To the research network esci / Springfield.

This repository contains resource documentation, usage guidelines and various blobs and script for the computational resources on the Springfield network.

`Resources <https://bitbucket.org/safl/springfield/src/master/resources/README.rst>`_.


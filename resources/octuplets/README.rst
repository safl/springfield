=============================
The Octuplets - octuplet01-08
=============================

An eight-node beowulf cluster for the Bohrium project as well as other related HPC research, teaching and development.

Consists of eight full-flegded computation nodes and a ninth scaled 
down frontend node for development / compilation. They are named::
  
  octuplets   / frontend
  octuplet01  / sashi
  octuplet02  / pria
  octuplet03  / anoop
  octuplet04  / gheet
  octuplet05  / poonam
  octuplet06  / uma
  octuplet07  / sandeep
  octuplet08  / nabendu  

Hardware
========

Components
----------

One node consists of the following hardware components::

  Motherboard 1   SuperMicro H8DG6-F,       MBD-H8DG6-F
  CPU         2   Opteron 6272,             OS6272WKTGGGUWOF
  Memory      16  Kingston 8GB REG-ECC,     KVR16R11D4/8HC
  Disks       1   OCZ Vertex Plus R2 120GB, VTXPLR2-25SAT2-120G
  GPU         3   MSI R7850 PE 2GB,         R7850 POWER EDITION 2GD5/OC  
  CPU-Cooler  2   Noctua NH-U9DO A3 2x92mm, NH-U9 DO A3
  PSU         1   Corsair 1050W,            CMPSU-1050HXEU
  Enclosure   1   HEC Compucase S401BS,     S401BS-atx  
  RACKKIT     1                             S-SR226K
  FAN12       3                             SY1225SL12SH
  FAN8        2                             AFACO-08000-GBA01

The enclosure becomes 4U due to the low-cost mainstream GPUs does not
fit into anything smaller. Additionally it must be support EATX (extended ATX) 12 in (305 mm) 13 in (330 mm).

========================
Manual Node Installation
========================

BIOS
====

...

Operating System
================

Ubuntu Server 12.04(.2) LTS.

Post Install
------------

Binary driver blobs and OpenCL SDK from AMD are available from::

  http://www2.ati.com/drivers/linux/amd-driver-installer-catalyst-12.10-x86.x86_64.zip
  http://developer.amd.com/wordpress/media/files/AMD-APP-SDK-v2.8-lnx64.tgz

However for convenience these along with a couple of other blobs/scripts are bundled into an archive on bitbucket.
The post-install commands below will setup/install:

  1) GPU drivers
  2) OpenCL AMD-SDK
  3) Burnin/test programs

The AMD drivers are dependant on X. It is preferred to install only a 
minimal X environment as to minimize interference with 
benchmark-timings.

Step 1 - System Update
~~~~~~~~~~~~~~~~~~~~~~

  sudo apt-get -y update
  sudo apt-get -y dist-upgrade
  sudo apt-get -y install \
  xorg xdm fluxbox devscripts dpkg-dev debhelper dh-modaliases libqtgui4 execstack \
  lm-sensors htop unzip \
  g++ clang llvm git vim \
  numactl linux-tools-common libboost-dev \
  python-setuptools python-numpy python-mako python-twisted python-opengl
  sudo apt-get -y autoremove
  sudo reboot

Step 2 - Driver Installation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # Grab the archive
  cd ~
  wget https://bitbucket.org/safl/springfield/get/master.tar.gz
  tar -xzf master.tar.gz
  mv safl-* install
  cd install/blobs
  
  # Install the driver
  unzip amd-driver-installer-catalyst-12.10-x86.x86_64.zip
  chmod +x amd-driver-installer-catalyst-12.10-x86.x86_64.run 
  sudo ./amd-driver-installer-catalyst-12.10-x86.x86_64.run 

  # Verify that the cards are discovered
  sudo aticonfig --list-adapters

  # Then create an Xorg config for all the adapters
  sudo aticonfig --adapter=all --initial
  sudo aticonfig --adapter=all --initial
  sudo aticonfig --adapter=all --initial
  
  # Fix Remote use / Device discovery without monitor attached.
  sudo sh -c 'echo "xhost +" >> /etc/X11/xdm/Xsetup'
  sudo sh -c 'echo "chmod uog+rw /dev/ati/card*" >> /etc/X11/xdm/Xsetup'
 
  #
  # PAY ATTENTION TO THIS COMMENT:
  #
  # Add:
  # case $DISPLAY in '') export DISPLAY=:0;; *) ;; esac
  # to: /etc/bash.bashrc

  # Reboot, this should be done since the remaining commands require
  # that the driver and stuff is correctly loaded and running.
  sudo reboot
  
Step 3 - OpenCL SDK and GPU clock-settings
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  # You should now have the three cards showing up:
  cd ~/install/blobs
  clinfo | grep GPU
    
  #
  # Downclock the cards to reference-board speeds.
  # This lowers heat-consumption in default use.
  #
  sudo aticonfig --od-enable
  sudo aticonfig --od-setclock=860,1200 --adapter=all
  sudo aticonfig --od-commitclocks
  
  # Install the SDK
  tar -xzf AMD-APP-SDK-v2.8-lnx64.tgz
  sudo ./Install-AMD-APP.sh
  sudo reboot
    
  #
  # If need be; then revert to the factory-overclocked state
  #
  # sudo aticonfig --od-enable
  # sudo aticonfig --od-setclock=950,1200 --adapter=all
  # sudo aticonfig --od-commitclocks

Reinstalling Drivers
====================

  sudo apt-get remove --purge fglrx fglrx_* fglrx-amdcccle* fglrx-dev*
  sudo 

Benchmarks / Burn-in
====================

FurMark, OpenCL, LAPACK.
------------------------

gputest from FurMark
https://github.com/spaffy/shoc/wiki

!!STREAM!!

http://www.cs.virginia.edu/stream/
wget http://www.cs.virginia.edu/stream/FTP/Code/stream.c

Assembly Log
============

Node 1
------

The following troublesome stuff occured::

  1 - Motherboard DOA: ...
  2 - Missing mounting-bracket for CPU cooler.
  3 - Connectors for CHASSIS-LED is incompatible with pin-out.
  4 - Heat seems to be an issue as the chassis does not come with fans. One solution is found: add three 120mm fans...

Node 2
------

  5 - Screw in CPU mounting-bracket cracked in assembly, rendered motherboard useless. One could possibly replace the mounting-plate on the motherboard: "BKT-0031L".

Node 3
------

No remarks.

Node 4
------

No remarks.

Node 5
------

No remarks.

Node 6
------

No remarks.

Node 7
------

No remarks.

Node 8
------

DCT1 on CPU-Node3 does not recognize the memory installed in P2-DIMM4A and P-2DIMM4B.
A total of 16 memory modules are installed 

One memory channel is not picking up on the DIMMs installed.

Information from BIOS Setup


Rack Installation
-----------------

The cluster will be installed in the 42U rack enclosure as illustrated below, in the figure each line represents a rack-unit::

    █▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀█
    █                   █
    █                   █
    █ ██ LARCH ████████ █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █
    █ ▀▀ SWITCH ▀▀▀▀▀▀▀ █
    █ █████████████████ █
    █ ██ OCTUPLET08 ███ █
    █ █████████████████ █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █
    █ █████████████████ █
    █ ██ OCTUPLET07 ███ █
    █ █████████████████ █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █
    █ █████████████████ █
    █ ██ OCTUPLET06 ███ █
    █ █████████████████ █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █
    █ █████████████████ █
    █ ██ OCTUPLET05 ███ █
    █ █████████████████ █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █
    █ █████████████████ █
    █ ██ OCTUPLET04 ███ █
    █ █████████████████ █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █
    █ █████████████████ █
    █ ██ OCTUPLET03 ███ █
    █ █████████████████ █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █
    █ █████████████████ █    
    █ ██ OCTUPLET02 ███ █    █████████████████████
    █ █████████████████ █    █                   █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █    █                   █
    █ █████████████████ █    █ █████████████████ █
    █ ██ OCTUPLET01 ███ █    █ ██ BURNS01   ████ █
    █ █████████████████ █    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █    █ █████████████████ █
    █ █████████████████ █    █ ██ WOLFCASTLE ███ █
    █ ██ OCTUPLETS ████ █    █ █████████████████ █
    █ █████████████████ █    █████████████████████
    █ ▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀ █    █                   █
    █                   █    █                   █
    █                   █    █                   █
    █████████████████████    █                   █
    

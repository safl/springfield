System Overview
~~~~~~~~~~~~~~~

Supermicro H8DG6/i(-F)
Version: 3.0
Build Date: 09/10/2012

Processor 6272
Speed: 2100Mhz

System Memory
Size: 131056MB

CPU Configuration
~~~~~~~~~~~~~~~~~

Module Version: OrochiPI 1.5.0.0-1
Socket Count: 2
Node Count: 4
Core Count: 32
HT Link Frequency: 2600Mhz

CPU Information (CPU Socket 1)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

AMD Opteron(TM) Processor 6272
Revision: B2
Cache L1: 768KB
Cache L2: 16348KB
Cache L3: 16MB
Speed: 2100Mhz, NB Clk: 2000Mhz
Able to Change Frequency: Yes
uCode Patch Level: 0x6000629

CPU Information (CPU Socket 1)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

AMD Opteron(TM) Processor 6272
Revision: B2
Cache L1: 768KB
Cache L2: 16348KB
Cache L3: 16MB
Speed: 2100Mhz, NB Clk: 2000Mhz
Able to Change Frequency: Yes
uCode Patch Level: 0x6000629

~~~~~~~~~~~~~~~~~~~~~~~~~
Advanced Chipset Settings
~~~~~~~~~~~~~~~~~~~~~~~~~

SR56x0 CIMx Version: 1.0.1.B
SP5100 CIMx Version: 6.7.0

Northbridge Chipset Configuration
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

HT Speed Support  [Auto]
IOMMU             [Disabled]

Memory Timing Parameters [CPU Node 0]
-------------------------------------

Memory Speed:             1600mhz,  1600mhz
CAS Latency (TCL):        11 CLK,   11 CLK
RAS/CAS Delay (Trcd):     11 CLK,   11 CLK
Row Precharge Time (TRP): 11 CLK,   11 CLK
Min Active RAS(Tras):     28 CLK,   28 CLK
RAS/RAS Delay (Trrd):     5  CLK,   5  CLK
Row Cycle (Trc):          39 CLK,   39 CLK
Read to Precharge (Trtp): 6  CLK,   6  CLK
Write Recover Time (Twr): 12 CLK,   12 CLK

Memory Timing Parameters [CPU Node 1]
-------------------------------------

Memory Speed:             1600mhz,  1600mhz
CAS Latency (TCL):        11 CLK,   11 CLK
RAS/CAS Delay (Trcd):     11 CLK,   11 CLK
Row Precharge Time (TRP): 11 CLK,   11 CLK
Min Active RAS(Tras):     28 CLK,   28 CLK
RAS/RAS Delay (Trrd):     5  CLK,   5  CLK
Row Cycle (Trc):          39 CLK,   39 CLK
Read to Precharge (Trtp): 6  CLK,   6  CLK
Write Recover Time (Twr): 12 CLK,   12 CLK

Memory Timing Parameters [CPU Node 2]
-------------------------------------

Memory Speed:             1600mhz,  1600mhz
CAS Latency (TCL):        11 CLK,   11 CLK
RAS/CAS Delay (Trcd):     11 CLK,   11 CLK
Row Precharge Time (TRP): 11 CLK,   11 CLK
Min Active RAS(Tras):     28 CLK,   28 CLK
RAS/RAS Delay (Trrd):     5  CLK,   5  CLK
Row Cycle (Trc):          39 CLK,   39 CLK
Read to Precharge (Trtp): 6  CLK,   6  CLK
Write Recover Time (Twr): 12 CLK,   12 CLK

Memory Timing Parameters [CPU Node 3]
-------------------------------------

Memory Speed:             1600mhz,  1600mhz
CAS Latency (TCL):        11 CLK,   11 CLK
RAS/CAS Delay (Trcd):     11 CLK,   11 CLK
Row Precharge Time (TRP): 11 CLK,   11 CLK
Min Active RAS(Tras):     28 CLK,   28 CLK
RAS/RAS Delay (Trrd):     5  CLK,   5  CLK
Row Cycle (Trc):          39 CLK,   39 CLK
Read to Precharge (Trtp): 6  CLK,   6  CLK
Write Recover Time (Twr): 12 CLK,   12 CLK

Memory Configuration
++++++++++++++++++++

Bank Interleaving     [Auto]
Node Interleaving     [Disabled]
Channel Interleaving  [Auto]
CS Sparing Enable     [Disabled]
Power Down Enable     [Enabled]
Bank Swizzle Mode     [Enabled]

ECC Configuration
+++++++++++++++++

ECC Mode        [Basic]

Memory Timing Configuration
++++++++++++++++++++++++++++

Memory Timing Config  [Auto]

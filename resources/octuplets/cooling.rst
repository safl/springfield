============================================================
Getting to grips with the cooling the octuplets at full load
============================================================

Experiments performed on octuplet01 / sashi.

GPU Core: 860           1200

Straight Through
================

This is the baseline experiment, trying to create a "straight" air-flow::
    
      +----------------+
  <-  |PSU  |          |
      |_____|          |
  <-  |F      CPU     F|
      |            <- A|
  <-  |F  CPU         N|
     F| GPU           F|
  <- A| GPU        <- A|
     N| GPU           N|
      +----------------+

Results::

  ┌─────────────────────────────────────────────────────────────────────────────────┐
  │                               --{ Burner v0.1 }--                               │
  │─────────────────────────────────────────────────────────────────────────────────│
  │ MB Sensors                                                                      │
  │ temp1 - 48.1                                                                    │
  │ temp1 - 48.5                                                                    │
  │ temp1 - 32.9                                                                    │
  │ temp1 - 32.9                                                                    │
  │ power1 - 112.95                                                                 │
  │ power1 - 113.17                                                                 │
  │                                                                                 │
  │ GPU Sensors                                                                     │
  │ 0 - AMD Radeon HD 7800 Series  - 93.00                                          │
  │ 1 - AMD Radeon HD 7800 Series  - 96.00                                          │
  │ 2 - AMD Radeon HD 7800 Series  - 66.00                                          │
  │                                                                                 │
  │ Elapsed: 17min 20sec                                                            │
  └─────────────────────────────────────────────────────────────────────────────────┘

  ┌─────────────────────────────────────────────────────────────────────────────────┐
  │                               --{ Burner v0.1 }--                               │
  │─────────────────────────────────────────────────────────────────────────────────│
  │ MB Sensors                                                                      │
  │ temp1 - 48.5                                                                    │
  │ temp1 - 48.6                                                                    │
  │ temp1 - 33.2                                                                    │
  │ temp1 - 33.1                                                                    │
  │ power1 - 112.58                                                                 │
  │ power1 - 113.01                                                                 │
  │                                                                                 │
  │ GPU Sensors                                                                     │
  │ 0 - AMD Radeon HD 7800 Series  - 94.00                                          │
  │ 1 - AMD Radeon HD 7800 Series  - 96.00                                          │
  │ 2 - AMD Radeon HD 7800 Series  - 67.00                                          │
  │                                                                                 │
  │ Elapsed: 25min 56sec                                                            │
  └─────────────────────────────────────────────────────────────────────────────────┘

The external fan does not suck much air away from the middle GPU since the "center" of the fan is situated here.
I expect this to be the cause of it having a the highest temperature.


Straight Through no PCI
=======================

      +----------------+
  <-  |PSU  |          |
      |_____|          |
  <-  |F      CPU     F|
      |            <- A|
  <-  |F  CPU         N|
     F  GPU           F|
  <- A  GPU        <- A|
     N  GPU           N|
      +----------------+

Results::

  ┌─────────────────────────────────────────────────────────────────────────────────┐
  │                               --{ Burner v0.1 }--                               │
  │─────────────────────────────────────────────────────────────────────────────────│
  │ MB Sensors                                                                      │
  │ temp1 - 39.5                                                                    │
  │ temp1 - 39.8                                                                    │
  │ temp1 - 29.5                                                                    │
  │ temp1 - 29.5                                                                    │
  │ power1 - 115.08                                                                 │
  │ power1 - 113.16                                                                 │
  │                                                                                 │
  │ GPU Sensors                                                                     │
  │ 0 - AMD Radeon HD 7800 Series  - 70.00                                          │
  │ 1 - AMD Radeon HD 7800 Series  - 61.00                                          │
  │ 2 - AMD Radeon HD 7800 Series  - 49.00                                          │
  │                                                                                 │
  │ Elapsed: 18min 29sec                                                            │
  └─────────────────────────────────────────────────────────────────────────────────┘


  ┌──────────────────────────────────────────────────────────────────────────────────────────────────────┐
  │                                          --{ Burner v0.1 }--                                         │
  │──────────────────────────────────────────────────────────────────────────────────────────────────────│
  │ MB Sensors                                                                                           │
  │ temp1 - 41.2                                                                                         │
  │ temp1 - 41.4                                                                                         │
  │ temp1 - 31.0                                                                                         │
  │ temp1 - 31.0                                                                                         │
  │ power1 - 113.26                                                                                      │
  │ power1 - 113.01                                                                                      │
  │                                                                                                      │
  │ GPU Sensors                                                                                          │
  │ 0 - AMD Radeon HD 7800 Series  - 75.00                                                               │
  │ 1 - AMD Radeon HD 7800 Series  - 67.00                                                               │
  │ 2 - AMD Radeon HD 7800 Series  - 55.00                                                               │
  │ Elapsed: 24min 40sec                                                                                 │
  └──────────────────────────────────────────────────────────────────────────────────────────────────────┘

Second run, after removing thermos from fans. The fans suck a lot more air out.
It does however not seems to have a positive effect on the temperature...

  ┌────────────────────────────────────────────┐
  │             --{ Burner v0.1 }--            │
  │────────────────────────────────────────────│
  │ MB Sensors                                 │
  │ power1 - 112.70                            │
  │ power1 - 113.09                            │
  │ temp1 - 43.0                               │
  │ temp1 - 43.2                               │
  │ temp1 - 31.8                               │
  │ temp1 - 31.8                               │
  │                                            │
  │ GPU Sensors                                │
  │ 0 - AMD Radeon HD 7800 Series  - 83.00     │
  │ 1 - AMD Radeon HD 7800 Series  - 72.00     │
  │ 2 - AMD Radeon HD 7800 Series  - 60.00     │
  │                                            │
  │ Elapsed: 9min 29sec                        │
  └────────────────────────────────────────────┘

It seems like it would be an idea to go back to TC... weird...

  ┌────────────────────────────────────────────┐
  │             --{ Burner v0.1 }--            │
  │────────────────────────────────────────────│
  │ MB Sensors                                 │
  │ power1 - 113.28                            │
  │ power1 - 113.17                            │
  │ temp1 - 45.0                               │
  │ temp1 - 45.1                               │
  │ temp1 - 33.2                               │
  │ temp1 - 33.1                               │
  │                                            │
  │ GPU Sensors                                │
  │ 0 - AMD Radeon HD 7800 Series  - 85.00     │
  │ 1 - AMD Radeon HD 7800 Series  - 74.00     │
  │ 2 - AMD Radeon HD 7800 Series  - 61.00     │
  │                                            │
  │ Elapsed: 21min 56sec                       │
  └────────────────────────────────────────────┘

Hmm or not... the room temperature had also risen significantly.

  ┌────────────────────────────────────────────┐
  │             --{ Burner v0.1 }--            │
  │────────────────────────────────────────────│
  │ MB Sensors                                 │
  │ power1 - 113.27                            │
  │ power1 - 113.06                            │
  │ temp1 - 44.5                               │
  │ temp1 - 44.5                               │
  │ temp1 - 32.9                               │
  │ temp1 - 32.6                               │
  │                                            │
  │ GPU Sensors                                │
  │ 0 - AMD Radeon HD 7800 Series  - 84.00     │
  │ 1 - AMD Radeon HD 7800 Series  - 73.00     │
  │ 2 - AMD Radeon HD 7800 Series  - 61.00     │
  │                                            │
  │ Elapsed: 29min 4sec                        │
  └────────────────────────────────────────────┘

Overall higher than initial runs however ambient temperatures account for that... i assume...

Straight Through 1-PCI
=======================

      +----------------+
  <-  |PSU  |          |
      |_____|          |
  <-  |F      CPU     F|
      |            <- A|
  <-  |F  CPU         N|
     F  GPU           F|
  <- A  GPU        <- A|
     N| GPU           N|
      +----------------+

┌───────────────────────────────────────────────────────┐
│                  --{ Burner v0.1 }--                  │
│───────────────────────────────────────────────────────│
│ MB Sensors                                            │
│ temp1 - 39.5                                          │
│ temp1 - 39.8                                          │
│ temp1 - 29.5                                          │
│ temp1 - 29.4                                          │
│ power1 - 36.16                                        │
│ power1 - 36.16                                        │
│                                                       │
│ GPU Sensors                                           │
│ 0 - AMD Radeon HD 7800 Series  - 81.00                │
│ 1 - AMD Radeon HD 7800 Series  - 72.00                │
│ 2 - AMD Radeon HD 7800 Series  - 60.00                │
│                                                       │
│                                                       │
│                                                       │
│ Elapsed: 24min 30sec                                  │
└───────────────────────────────────────────────────────┘

┌─────────────────────────────────────────────┐
│             --{ Burner v0.1 }--             │
│─────────────────────────────────────────────│
│ MB Sensors                                  │
│ temp1 - 25.1                                │
│ temp1 - 24.9                                │
│ temp1 - 17.5                                │
│ temp1 - 17.9                                │
│ power1 - 44.93                              │
│ power1 - 42.63                              │
│                                             │
│ GPU Sensors                                 │
│ 0 - AMD Radeon HD 7800 Series  - 89.00      │
│ 1 - AMD Radeon HD 7800 Series  - 72.00      │
│ 2 - AMD Radeon HD 7800 Series  - 60.00      │
│                                             │
│ Elapsed: 50min 41sec                        │
└─────────────────────────────────────────────┘


Straight Throu

Straight Through Modified Brackets on octuplet04
================================================

This is the layout::

      +----------------+
  <-  |PSU  |          |
      |_____|          |
  <-  |F      CPU     F|
      |            <- A|
  <-  |F  CPU         N|
     F{ GPU           F|
  <- A{ GPU        <- A|
     N{ GPU           N|
      +----------------+

Results::

┌──────────────────────────────────────────────────┐
│                --{ Burner v0.1 }--               │
│──────────────────────────────────────────────────│
│ MB Sensors                                       │
│ temp1 - 37.0                                     │
│ temp1 - 36.0                                     │
│ temp1 - 30.0                                     │
│ temp1 - 30.0                                     │
│ power1 - 36.16                                   │
│ power1 - 36.16                                   │
│                                                  │
│ GPU Sensors                                      │
│ 0 - AMD Radeon HD 7800 Series  - 81.00           │
│ 1 - AMD Radeon HD 7800 Series  - 77.00           │
│ 2 - AMD Radeon HD 7800 Series  - 52.00           │
│                                                  │
│ Elapsed: 42min 46sec                             │
└──────────────────────────────────────────────────┘


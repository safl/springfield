============================
Node Management / Deployment
============================

- Ubuntu MaaS / Cobbler http://cobbler.github.com/about.html
- puppet http://projects.puppetlabs.com/projects/puppet

- slurm https://computing.llnl.gov/linux/slurm/
- MiG

Ubuntu 12.04.1 LTS
==================

Post-install
------------

  # Configure network interfaces
  
  # Management Network
  autho eth0
  interface eth0 inet static
  address 10.10.0.1
  netmask 255.255.255.0
  
  # NBI LAN
  auto eth1
  interface eth1 inet dhcp
  
  # Update System
  sudo apt-get -y update
  sudo apt-get -y upgrade

  # Install MaaS
  sudo apt-get -y install vim git maas maas-dhcp maas-provision maas-enlist
  sudo maas createsuperuser
  sudo maas-import-isos

Changing IP on MAAS-server
--------------------------

After changing IP by modifying /etc/network/interfaces you need to do the following to propagate the change::

  sudo rm -f /var/lib/cobbler/config/profiles.d/*.json
  sudo dpkg-reconfigure maas
  sudo dpkg-reconfigure maas-dhcp
  sudo dpkg-reconfigure maas-provision
  sudo maas-import-isos --update-settings
  
This however does not seem to be sufficient... fucked up!

MaaS is annoying... proven cobbler is used instead.

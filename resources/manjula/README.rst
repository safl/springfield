=================
Manjula - manjula
=================

Compile node for the octuplet cluster.

Hardware
========

The machine consists of the following components::

  Motherboard 1   SuperMicro H8SGL-F,       MBD-H8SGL-F
  CPU         1   Opteron 6234,             
  Memory      4   Kingston 8GB REG-ECC,     KVR16R11D4/8HC
  Disks       1   OCZ Vertex Plus R2 120GB, VTXPLR2-25SAT2-120G
  GPU         1   SAPPHIRE RADEON HD7850 OC 2GB, 
  CPU-Cooler  1   Noctua NH-U9DO A3 2x92mm, NH-U9DO A3
  PSU         1   Corsair HX650W,           CP-9020030-EU
  Enclosure   1   HEC Compucase S401BS,     S401BS-atx  
  RACKKIT     1                             S-SR226K
  FAN12       1                             SY1225SL12SH
  FAN8        1                             AFACO-08000-GBA01

octuplet08 / nabendu

eth0 - 00:25:90:A6:3E:96  -> Deployment / Management
eth1 - 00:25:90:A6:3E:97  -> External NIC
IPMI - 00:25:90:A6:3E:09  -> IPMI Interface

Apu14
=====

NFS-server with homedirs...


Packages::

  sudo apt-get install \
  htop vim git lynx \
  python-dev python-setuptools \
  nfs-kernel-server puppet
  
Setting up NFS-Server
---------------------

Do this::

  # Setup the export-filesystem
  sudo mkdir -p /export/users 
  
Bind this to home on boot::

  sudo vim /etc/fstab
  
By adding::

  /home/    /export/users   none    bind  0  0

Configure the idmapd::

  sudo vim /etc/idmapd.conf
  
Ensure domain is the same on client and server::

  Domain = escience.local

Mount manually with the command::

  sudo mount --bind /home/ /export/users
  
Configure the nfs-daemon::

  sudo vim /etc/default/nfs-kernel-server
  
Set::

  NEED_SVCGSSD=no # no is default
  RPCMOUNTDOPTS="--manage-gids --port 4000"
  
And::

  sudo vim /etc/default/nfs-common

Set::

  NEED_IDMAPD=yes
  NEED_GSSD=no # no is default

Configure lock-module::

  sudo vim /etc/modprobe.d/options
  
Set::

  options lockd nlm_udpport=4001 nlm_tcpport=4001
  options nfs callback_tcpport=4002

Make the module loadable::

  sudo vim /etc/modules
  
By appending::

  lockd

Edit::

  sudo vim /etc/exports
  
Set::

  /export       10.10.0.0/24(rw,fsid=0,insecure,no_subtree_check,async)
  /export/users 10.10.0.0/24(rw,nohide,insecure,no_subtree_check,async)

Edit::

  sudo vim /etc/hosts.deny

Set::

  rpcbind mountd nfsd statd lockd rquotad : ALL
  
Edit::

  sudo vim /etc/hosts.allow
  
Set::

   rpcbind mountd nfsd statd lockd rquotad : 10.10.0.0/24


Take a look at which ports it is currently using::

  rpcinfo -p

Add nfs-specific firewall rules::

  sudo vim /etc/network/if-pre-up.d/firewall
  
With the rules::

  # Allow access to nfs-server via LAN-interface
  iptables -A INPUT -p tcp -i eth1 --dport 111 -j ACCEPT
  iptables -A INPUT -p udp -i eth1 --dport 111 -j ACCEPT
  iptables -A INPUT -p tcp -i eth1 --dport 2049 -j ACCEPT
  iptables -A INPUT -p udp -i eth1 --dport 2049 -j ACCEPT
  iptables -A INPUT -p tcp -i eth1 --dport 4000 -j ACCEPT
  iptables -A INPUT -p udp -i eth1 --dport 4000 -j ACCEPT

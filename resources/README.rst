=====================
Springfield Resources
=====================


Wolfcastle
==========

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/wolfcastle/wolfcastle.png

Akira
=====

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/akira/akira.png

Otto
====

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/otto/otto.png


Skinner
=======

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/skinner/skinner.png

Milhouse
========

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/milhouse/milhouse.png

Krusty
======

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/krusty/krusty.png

Lisa
====

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/lisa/lisa.png

Nelson
======

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/nelson/nelson.png

Moleman
=======

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/moleman/moleman.png

Marge
=====

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/marge/marge.png

Octuplets
=========

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/octuplets/octuplets.png


Apu
===

.. image:: https://bitbucket.org/safl/springfield/raw/master/resources/apu/apu.png


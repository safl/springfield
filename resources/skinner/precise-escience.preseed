### Console
#
d-i     debian-installer/locale string en_US.UTF-8
d-i     debian-installer/splash boolean false
d-i     console-setup/ask_detect        boolean false
d-i     console-setup/layoutcode        string us
d-i     console-setup/variantcode       string 

### Network
#
d-i     netcfg/get_nameservers  string 
d-i     netcfg/get_ipaddress    string 
d-i     netcfg/get_netmask      string 255.255.255.0
d-i     netcfg/get_gateway      string 
d-i     netcfg/confirm_static   boolean true

#
# Any hostname and domain names assigned from dhcp take precedence over
# values set here.
# However, setting the values still prevents the questions from being shown, 
# even if values come from dhcp.
#
d-i netcfg/get_hostname string unassigned-hostname
d-i netcfg/get_domain string unassigned-domain

### MISC
#
# Disable that annoying WEP key dialog.
#
d-i netcfg/wireless_wep string

### Clock
#
d-i     clock-setup/utc boolean true
d-i     clock-setup/ntp boolean true
d-i     clock-setup/ntp-server  string dk.pool.ntp.org
d-i     time/zone               string Europe/Copenhagen

### Partitioning
#
d-i     partman-auto/method string regular

#
# If one of the disks that are going to be automatically partitioned
# contains an old LVM configuration, the user will normally receive a
# warning. This can be preseeded away...
#
d-i     partman-lvm/device_remove_lvm   boolean true

#
# The same applies to pre-existing software RAID array:
#
d-i     partman-md/device_remove_md     boolean true

#
# And the same goes for the confirmation to write the lvm partitions.
#
d-i     partman-lvm/confirm             boolean true

#
# You can choose one of the three predefined partitioning recipes:
# - atomic: all files in one partition
# - home:   separate /home partition
# - multi:  separate /home, /usr, /var, and /tmp partitions
#
d-i     partman-auto/choose_recipe select atomic
d-i     partman/default_filesystem string ext4

#
# This makes partman automatically partition without confirmation, provided
# that you told it what to do using one of the methods above.
#
d-i     partman-partitioning/confirm_write_new_label boolean true
d-i     partman/choose_partition select Finish
d-i     partman/confirm boolean true
d-i     partman/confirm_nooverwrite boolean true

### Base system installation
#
d-i     base-installer/kernel/image     string linux-server

### Account setup
#

### Custom eScience User Setup
#
# The encrypted password string can be generated with the python snippet:
# python -c 'import crypt; print crypt.crypt("MYPASSWORD", "$6$.1eHH0iY$")'
# http://alotofbytes.blogspot.com/2011/01/how-to-generate-encrypted-shadow.html
#
d-i     passwd/root-login       boolean true
d-i     passwd/root-password-crypted password $6$.1eHH0iY$tc1LUJrGxTGy4a5kAtyELVV8cuunog9Mxb/q0mRjVxxTYVaKFBb/FW3siKtBl/3Sb9ktsNncA2AXUQ4iAyAUl/
d-i     passwd/make-user        boolean false
d-i     user-setup/allow-password-weak  boolean false
d-i     user-setup/encrypt-home boolean false
d-i     passwd/user-default-groups      string adm cdrom dialout lpadmin plugdev sambashare

### Apt setup
#
# You can choose to install restricted and universe software, or to install
# software from the backports repository.
#
d-i     apt-setup/restricted boolean true
d-i     apt-setup/universe boolean true
d-i     apt-setup/backports boolean true

#
# Select which update services to use; define the mirrors to be used.
# Values shown below are the normal defaults.
#
d-i     apt-setup/services-select       multiselect security
d-i     apt-setup/security_host         string  security.ubuntu.com
d-i     apt-setup/security_path         string  /ubuntu

#
# Use the apt-cache-ng
#
d-i mirror/suite string precise
d-i mirror/country string manual
d-i mirror/protocol string http
d-i mirror/http/hostname string $http_server:3142
d-i mirror/http/directory string /ubuntu/
d-i mirror/http/proxy string

#
# By default the installer requires that repositories be authenticated
# using a known gpg key. This setting can be used to disable that
# authentication. Warning: Insecure, not recommended.
#
d-i     debian-installer/allow_unauthenticated  string false

### Package selection
#
d-i     pkgsel/include          string openssh-server puppet nfs-common lynx curl wget vim git screen htop unzip
d-i     pkgsel/upgrade          select  safe-upgrade
d-i     pkgsel/language-packs   multiselect en

#
# Policy for applying updates. May be "none" (no automatic updates),
# "unattended-upgrades" (install security updates automatically), or
# "landscape" (manage system with Landscape).
#
d-i     pkgsel/update-policy    select  none

#
# By default, the system's locate database will be updated after the
# installer has finished installing most packages. This may take a while, so
# if you don't want it, you can set this to "false" to turn it off.
#
d-i     pkgsel/updatedb         boolean true

#
# Some versions of the installer can report back on what software you have
# installed, and what software you use. The default is not to report back,
# but sending reports helps the project determine what software is most
# popular and include it on CDs.
#
popularity-contest popularity-contest/participate boolean false

### Boot loader installation
#
d-i     grub-installer/skip             boolean false
d-i     lilo-installer/skip             boolean false
d-i     grub-installer/only_debian      boolean true
d-i     grub-installer/with_other_os    boolean true

### Finishing up the installation
# During installations from serial console, the regular virtual consoles
# (VT1-VT6) are normally disabled in /etc/inittab. Uncomment the next
# line to prevent this.
d-i     finish-install/keep-consoles    boolean false

#
# Avoid that last message about the install being complete.
#
d-i     finish-install/reboot_in_progress       note 

#
# This will make the installer eject the CD during the reboot,
#
d-i     cdrom-detect/eject      boolean true

#
# This will reboot the machine instead of powering off / halting.
#
d-i     debian-installer/exit/halt      boolean false
d-i     debian-installer/exit/poweroff  boolean false

d-i     preseed/late_command string true && \
        unset HTTP_PROXY && \
        wget "http://$http_server:$http_port/cblr/svc/op/nopxe/system/$system_name" -O /dev/null && \
        in-target /bin/sed -i -e 's/START=no/START=yes/g' /etc/default/puppet && \
        true

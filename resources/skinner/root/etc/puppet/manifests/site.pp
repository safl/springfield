# /etc/puppet/manifests/site.pp

define pip_install ($filename, $ensure='present') {

	case $ensure {

		default: {
			err ( "unknown ensure value ${ensure}" )
		}
		absent: {
			err ( "Sorry i do not know how to ensure '${ensure}'." )
		}
		present: {

			file { "/tmp/$filename":
				source => "puppet:///files/$filename",
				ensure => present,
				before => Exec["pip_$filename"]
			}

			exec {	"pip_$filename":
				command => "pip install /tmp/$filename",
				path => [
					'/usr/local/bin',
					'/usr/bin'
				],
				logoutput => true

			}

		}

	}

}

define kern_module ($ensure) {
    $modulesfile = $operatingsystem ? { ubuntu => '/etc/modules', debian => "/etc/modules", redhat => "/etc/rc.modules", centos=>"/etc/rc.modules" }
    case $operatingsystem {
        redhat: { file { "/etc/rc.modules": ensure => file, mode => 755 } }
        centos: { file { "/etc/rc.modules": ensure => file, mode => 755 } }
    }
    case $ensure {
        present: {
            exec { "insert_module_${name}":
                command => $operatingsystem ? {
                    ubuntu => "/bin/echo '${name}' >> '${modulesfile}'",
                    debian => "/bin/echo '${name}' >> '${modulesfile}'",
                    redhat => "/bin/echo '/sbin/modprobe ${name}' >> '${modulesfile}' ",
                    centos => "/bin/echo '/sbin/modprobe ${name}' >> '${modulesfile}' "
                },
                unless => $operatingsystem ? {
                    ubuntu => "/bin/grep -qFx '${name}' '${modulesfile}'",
                    debian => "/bin/grep -qFx '${name}' '${modulesfile}'",
                    redhat => "/bin/grep -q '^/sbin/modprobe ${name}\$' '${modulesfile}'",
                    centos => "/bin/grep -q '^/sbin/modprobe ${name}\$' '${modulesfile}'",
                }
            }
            exec { "/sbin/modprobe ${name}": unless => "/bin/grep -q '^${name} ' '/proc/modules'" }
        }
        absent: {
            exec { "/sbin/modprobe -r ${name}": onlyif => "/bin/grep -q '^${name} ' '/proc/modules'" }
            exec { "remove_module_${name}":
                command => $operatingsystem ? {
                    ubuntu => "/usr/bin/perl -ni -e 'print unless /^\\Q${name}\\E\$/' '${modulesfile}'",
                    debian => "/usr/bin/perl -ni -e 'print unless /^\\Q${name}\\E\$/' '${modulesfile}'",
                    redhat => "/usr/bin/perl -ni -e 'print unless /^\\Q/sbin/modprobe ${name}\\E\$/' '${modulesfile}'",
                    centos => "/usr/bin/perl -ni -e 'print unless /^\\Q/sbin/modprobe ${name}\\E\$/' '${modulesfile}'"
                },
                onlyif => $operatingsystem ? {
                    ubuntu => "/bin/grep -qFx '${name}' '${modulesfile}'",
                    debian => "/bin/grep -qFx '${name}' '${modulesfile}'",
                    redhat => "/bin/grep -q '^/sbin/modprobe ${name}\$' '${modulesfile}'",
                    centos => "/bin/grep -q '^/sbin/modprobe ${name}\$' '${modulesfile}'"
                }
            }
        }
        default: { err ( "unknown ensure value ${ensure}" ) }
    }
}


define line($file, $line, $ensure = 'present') {
    case $ensure {
        default : { err ( "unknown ensure value ${ensure}" ) }
        present: {
            exec { "/bin/echo '${line}' >> '${file}'":
                unless => "/bin/grep -qFx '${line}' '${file}'"
            }
        }
        absent: {
            exec { "/bin/grep -vFx '${line}' '${file}' | /usr/bin/tee '${file}' > /dev/null 2>&1":
              onlyif => "/bin/grep -qFx '${line}' '${file}'"
            }

            # Use this resource instead if your platform's grep doesn't support -vFx;
            # note that this command has been known to have problems with lines containing quotes.
            # exec { "/usr/bin/perl -ni -e 'print unless /^\\Q${line}\\E\$/' '${file}'":
            #     onlyif => "/bin/grep -qFx '${line}' '${file}'"
            # }
        }
    }
}

import "users"
import "nodes"

class krusty_packages {

	package {[
		'cython',
		'python2.6',
		'python2.7',
		'python3',
		'python-dev',
		'python-pip',
		'python-numpy',
		'python-scipy',			
		'python-matplotlib',		
		'build-essential',		
		'curl',
		'mpich2',
		'expect',

		]:ensure => installed
	}

	package {[
    'powernap',
    'powernap-common'
    ]:ensure => absent
	}

}

class krusty_pymods {

	pip_install { "pastset107":
		filename => 'pastset-1.0.7.zip'
	}

	pip_install { "pupympi097":
		filename => 'pupyMPI-0.9.7.tar.gz'
	}

}

class krusty_hosts {
	file { "/etc/hosts":
		owner	=> root,
		group	=> root,
		mode	=> 775,
		source	=> "puppet:///files/krusty_hosts"
	}
}

class octuplet_packages {

	package {[		
		'python2.6', 'python2.7', 'python3', 'python-dev', 'python-pip',
		'python-numpy', 'python-scipy', 'python-matplotlib',
    'python-setuptools', 'python-numpy', 'python-mako', 'python-twisted', 'python-opengl',
    'cython',
		
		'curl', 'wget', 'lynx', 'expect', 'screen', 'htop'
    'numactl', 'lm-sensors', 'htop', 'unzip', 
    
    'devscripts', 'dpkg-dev', 'debhelper', 'dh-modaliases', 'libqtgui4', 'execstack',    
    'build-essential', 'linux-tools-common', 'libboost-dev', 'mpich2',
    
    'g++', 'clang', 'llvm',
    'git', 'vim',
    
    'xorg', 'xdm, 'fluxbox',

		]:ensure => installed
	}

	package {[
		'powernap',
		'powernap-common'
		]:ensure => absent
	}

}

class nfs_homes {

	package { 'nfs-common':
		ensure => installed,
		before	=> Mount["/home"],
	}
	file { "/etc/modules":
		ensure => present,
		before	=> Mount["/home"],
	}
	line {	nfs_module:
		file => "/etc/modules",
		line => "nfs",
		before	=> Mount["/home"],
	}
	file { "/etc/default/nfs-common":
		ensure	=> present,
		content	=> template("nfs-common.erb"),
		before	=> Mount["/home"],
		
	}	
	kern_module {
		"nfs": ensure => present,
		before	=> Mount["/home"],
	}

	mount { "/home":
		device  => "krusty:/users",
		fstype  => "nfs4",
		ensure  => "mounted",
		options => "_netdev,auto",
		atboot  => true,
		subscribe  => [File['/etc/modules'], File['/etc/default/nfs-common']]
	}

}

node 'krusty-lan.krusty.local'	{ include escience_users }
node 'n0.krusty.local'          { include escience_users include krusty_packages include nfs_homes include krusty_hosts include krusty_pymods }
node 'n1.krusty.local'          { include escience_users include krusty_packages include nfs_homes include krusty_hosts include krusty_pymods }
node 'n2.krusty.local'          { include escience_users include krusty_packages include nfs_homes include krusty_hosts include krusty_pymods }
node 'n3.krusty.local'          { include escience_users include krusty_packages include nfs_homes include krusty_hosts include krusty_pymods }
node 'n4.krusty.local'          { include escience_users include krusty_packages include nfs_homes include krusty_hosts include krusty_pymods }
node 'n5.krusty.local'          { include escience_users include krusty_packages include nfs_homes include krusty_hosts include krusty_pymods }
node 'n6.krusty.local'          { include escience_users include krusty_packages include nfs_homes include krusty_hosts include krusty_pymods }
node 'n7.krusty.local'          { include escience_users include krusty_packages include nfs_homes include krusty_hosts include krusty_pymods }

node 'manjula.sprungfeld.local'       { include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }
node 'octuplet01.sprungfeld.local'		{ include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }
node 'octuplet02.sprungfeld.local'		{ include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }
node 'octuplet03.sprungfeld.local'		{ include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }
node 'octuplet04.sprungfeld.local'		{ include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }
node 'octuplet05.sprungfeld.local'		{ include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }
node 'octuplet06.sprungfeld.local'		{ include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }
node 'octuplet07.sprungfeld.local'		{ include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }
node 'octuplet08.sprungfeld.local'		{ include escience_users include octuplet_packages include nfs_homes include octuplet_hosts }

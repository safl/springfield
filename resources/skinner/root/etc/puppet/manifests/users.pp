# /etc/puppet/manifests/users.pp

class escience_users {

	ssh_authorized_key {
		"root_krusty_pub":
		ensure	=> present,
		key	=> 'AAAAB3NzaC1yc2EAAAADAQABAAABAQCuPodkfsUEapBYc9GbyuGJdZ7LvyiAL8He4+Hj+Ub6E4lFPe5/kuoXLQMAy9P8j2SmjRR1Jfk1s6itK8FcL4/EdfL3IQnfImJ9a1AAp/po6jmyyGD2MIBiVxAdE80skIoQ/QTCo9A49SFBZip5JfYijmBnTf9bIgHN5zHOPPTEmbJcwU9tkHkaNrYgyu4f3Nh2Pgw5wrWJx3UZagRILEz3tnIJB58sb1Q50fNZNEMH1CUK1CY3KB4RYnZJvSjZ1iwEuocFQcV991vF/Js1qatbwa+ZB+9gXRJV46OadyE4FG1Ze/FNXTkD8SanNc7a3gA7B0XNE+IasBwAyNJFrskf',
		name	=> 'root@krusty',
		type	=> 'ssh-rsa',
		user	=> root
	}

	group {"admin": ensure=>present}

	group {	"bardino":
		ensure	=> present,
		gid	=> 2000,
	}
	user {	"bardino":
		ensure	=> present,
		uid	=> 2000,
		gid	=> 2000,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/bardino'
	}

	group {	"madsbk":
		ensure	=> present,
		gid	=> 2001,
	}
	user {	"madsbk":
		ensure	=> present,
		uid	=> 2001,
		gid	=> 2001,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/madsbk'
	}

	group {	"rehr":
		ensure	=> present,
		gid	=> 2002,
	}
	user {	"rehr":
		ensure	=> present,
		uid	=> 2002,
		gid	=> 2002,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/rehr'
	}

	group {	"vinter":
		ensure	=> present,
		gid	=> 2003,
	}
	user {	"vinter":
		ensure	=> present,
		uid	=> 2003,
		gid	=> 2003,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/vinter'
	}

	group {	"safl":
		ensure	=> present,
		gid	=> 2004,
	}
	user {	"safl":
		ensure	=> present,
		uid	=> 2004,
		gid	=> 2004,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/safl'
	}

	group {	"mig-re":
		ensure	=> present,
		gid	=> 2005
	}
	user {	"mig-re":
		ensure	=> present,
		uid	=> 2005,
		gid	=> 2005,
		shell	=> '/bin/bash',
		home	=> '/home/mig-re'
	}

	group {	"mig":
		ensure	=> present,
		gid	=> 2006
	}
	user {	"mig":
		ensure	=> present,
		uid	=> 2006,
		gid	=> 2006,
		groups	=> ['mig-re'],
		shell	=> '/bin/bash',
		home	=> '/home/mig'
	}
  
  group {	"troels":
		ensure	=> present,
		gid	=> 2007
	}
	user {	"troels":
		ensure	=> present,
		uid	=> 2007,
		gid	=> 2007,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/troels'
	}
  
  group {	"kenneth":
		ensure	=> present,
		gid	=> 2008
	}
	user {	"kenneth":
		ensure	=> present,
		uid	=> 2008,
		gid	=> 2008,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/kenneth'
	}
  
  group {	"avery":
		ensure	=> present,
		gid	=> 2009
	}
	user {	"avery":
		ensure	=> present,
		uid	=> 2009,
		gid	=> 2009,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/avery'
	}
  
  group {	"weifeng":
		ensure	=> present,
		gid	=> 2010
	}
	user {	"weifeng":
		ensure	=> present,
		uid	=> 2010,
		gid	=> 2010,
		groups	=> ['admin'],
		shell	=> '/bin/bash',
		home	=> '/home/weifeng'
	}

}

===========================
Principal Skinner - skinner
===========================

Cobbler + Dnsmasq + Puppet + apt-cacher-ng + network gateway + homedirs (nfs-server)

The machine should have two network interfaces eth0 and eth1:

  * eth0 must be connected to the "int / management / escience.local" network.
  * eth1 must be connected to the "ext / internet / nbi lan" network.

Ubuntu 12.04.1 LTS
==================

Post-install::

  sudo apt-get -y update
  sudo apt-get -y upgrade
  sudo apt-get install ufw
  sudo reboot

Network Configuration and Gateway
---------------------------------

Edit::

  sudo vim /etc/network/interfaces
  
Should contain::

  auto lo
  iface lo inet loopback

  #
  # The "escience.local" network.
  #
  # Maybe this should be named "sprungfeld" or something along
  # those lines...
  #
  auto eth0
  iface eth0 inet static
    address 10.10.0.1
    netmask 255.255.255.0
    gateway 10.10.0.1

  #
  # Keep in sync with the "eScience NBI IPs" document.
  #
  auto eth1
  iface eth1 inet static
    address 130.225.212.173
    netmask 255.255.254.0
    gateway 130.225.212.1
    dns-nameservers 130.225.212.46 130.225.212.55
    dns-domain esci.nbi.dk
    dns-search esci.nbi.dk 
  
Then::

  sudo service networking restart

Enable packet forwarding::

  sudo vim /etc/sysctl.conf
  
Make sure it contains, in uncommented form::

  net.ipv4.ip_forward=1

Enable nat by editing::

  sudo vim /etc/rc.local
  
Insert on lines before "exit 0"::

  /sbin/iptables -P FORWARD ACCEPT
  /sbin/iptables --table nat -A POSTROUTING -o eth1 -j MASQUERADE
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 2201 -j DNAT --to-destination 10.10.0.101:22
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 2201 -j DNAT --to-destination 10.10.0.102:22
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 2201 -j DNAT --to-destination 10.10.0.103:22
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 2201 -j DNAT --to-destination 10.10.0.104:22
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 2201 -j DNAT --to-destination 10.10.0.105:22
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 2201 -j DNAT --to-destination 10.10.0.106:22
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 2201 -j DNAT --to-destination 10.10.0.107:22
  /sbin/iptables -t nat -A PREROUTING -i eth1 -p tcp --dport 2201 -j DNAT --to-destination 10.10.0.108:22

Packages et al
--------------

Procede with installing packages::

  sudo apt-get install \
  htop vim git lynx \
  mkisofs createrepo make debmirror \
  python-dev python-setuptools nmap tcpdump \
  cobbler cobbler-web dnsmasq \
  apt-cacher-ng puppetmaster pdsh ipmitool \
  nfs-kernel-server 

Configuring AptCacherNg
-----------------------

Make sure we have sufficient backends::

  sudo vim /etc/apt-cacher-ng/backends_ubuntu

It should contain the lines::
  
  http://www.mirrorservice.org/sites/archive.ubuntu.com/ubuntu/
  http://dk.archive.ubuntu.com/ubuntu/
  
Skinner should also eat his own medicine and also use the cache::

  sudo vim /etc/apt/apt.conf

This file is probably empty since Ubuntu manages non-user confs via the conf.d directory, so just populate the apt.conf with::

  Acquire::http { Proxy "http://127.0.0.1:3142"; };

Configuring Cobbler / Dnsmasq
-----------------------------

Then cobbler that it should manage dhcp/dns::

  sudo vim /etc/cobbler/settings

By setting::

  manage_dhcp: 1  
  manage_dns: 1
  manage_tftpd: 1
  restart_dhcp: 1
  restart_dns: 1
  next_server: 10.10.0.1
  pxe_just_ince: 1
  server: 10.10.0.1

Then tell cobbler what it should use dnsmasq conf to mange dhcp/dns::

  sudo vim /etc/cobbler/modules.conf
  
By setting::

  [dns]
  module = manage_dnsmasq
  
  [dhcp]
  module = manage_dnsmasq
  
  [tftpd]
  module = manage_in_tftpd

Then tell dnsmasq::

  sudo vim /etc/cobbler/dnsmasq.template
  
To restricts DHCP to the "internal" interface::

  # 
  # IMPORTANT:
  # Without this option, dnsmasq will dhcp-spam the external net!
  #
  except-interface=eth1
  dhcp-range=10.10.0.100,10.10.200
  
  # Octuplet IPMI interfaces
  dhcp-host=00:25:90:a4:27:05,10.10.0.111
  dhcp-host=00:25:90:a6:3e:1c,10.10.0.112
  dhcp-host=00:25:90:a4:27:23,10.10.0.113
  dhcp-host=00:25:90:a4:27:64,10.10.0.114
  dhcp-host=00:25:90:a4:26:e1,10.10.0.115
  dhcp-host=00:25:90:a4:26:e5,10.10.0.116
  dhcp-host=00:25:90:a2:c6:7b,10.10.0.117
  dhcp-host=00:25:90:a6:3e:09,10.10.0.118

  # apu14
  dhcp-host=64:70:02:01:d9:89,apu14,10.10.0.20

Commands::

  sudo cobbler sync
  sudo cobbler check
  sudo service cobbler restart

Update Cobbler-Web Auth / htdigest
----------------------------------

Run::

  htdigest /etc/cobbler/users.digest "Cobbler" cobbler

Adding/Importing Distributions
------------------------------

Importing Ubuntu Server 12.04.1 64bit::

  # Download the iso, choose an appropriate mirror from: https://launchpad.net/ubuntu/+cdmirrors
  mkdir ~/iso
  cd ~/iso
  wget http://ftp.klid.dk/ftp/ubuntu-cd/12.04.1/ubuntu-12.04.1-server-amd64.iso
  
  # Loop mount the ISO
  sudo mount -o loop ~/iso/ubuntu-12.04.1-server-amd64.iso /mnt
  
  # Import it!
  sudo cobbler import --name=ubuntu-server-12.04.1-amd64 --path=/mnt --breed=ubuntu

Create/Edit a profile
---------------------

A profile is operating system + various options such as preseed file and kernel options, the command below will attach preseed-file for automated-install and boot with the kernel options:

1. priority=critical
2. locale=en_US
3. netcfg/choose_interface=eth0

The third option is required for machines with multiple interfaces, othervise the installer will stop and prompt for the user to choose which nic to use.
A clean approach is to add: "d-i netcfg/choose_interface select auto" or "d-i netcfg/choose_interface select eth0", however this does not work for debian based distributions since the preseed file is shipped AFTER the network goes up.
The "add-profile" command::

  # Attach a pre-seed
  sudo cobbler profile edit --name=ubuntu-server-12.04.1-x86_64 --kickstart=/var/lib/cobbler/kickstarts/precise-escience.preseed --kopts="priority=critical locale=en_US netcfg/choose_interface=eth0"

Adding Machines
---------------

The following will add a machine binding a profile to the machines mac-address::

  # Adding the krusty machines
  sudo cobbler system add --name=krusty   --profile=ubuntu-server-12.04.1-x86_64 --mac= --interface=eth0 --ip-address=10.10.0.200 --dns-name="krusty.escience.local"
  sudo cobbler system add --name=krusty01 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:23:7D:48:B8:F5 --interface=eth0 --ip-address=10.10.0.201 --dns-name="krusty01.escience.local"
  sudo cobbler system add --name=krusty02 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:23:7D:49:EC:F5 --interface=eth0 --ip-address=10.10.0.202 --dns-name="krusty02.escience.local"
  sudo cobbler system add --name=krusty03 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:23:7D:49:ED:C4 --interface=eth0 --ip-address=10.10.0.203 --dns-name="krusty03.escience.local"
  sudo cobbler system add --name=krusty04 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:23:7D:48:B8:B7 --interface=eth0 --ip-address=10.10.0.204 --dns-name="krusty04.escience.local"
  sudo cobbler system add --name=krusty05 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:23:7D:48:B7:A0 --interface=eth0 --ip-address=10.10.0.205 --dns-name="krusty05.escience.local"
  sudo cobbler system add --name=krusty06 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:22:64:B6:24:72 --interface=eth0 --ip-address=10.10.0.206 --dns-name="krusty06.escience.local"
  sudo cobbler system add --name=krusty07 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:23:7D:49:E9:61 --interface=eth0 --ip-address=10.10.0.207 --dns-name="krusty07.escience.local"
  sudo cobbler system add --name=krusty08 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:23:7D:48:F3:9A --interface=eth0 --ip-address=10.10.0.208 --dns-name="krusty08.escience.local"

  # Adding the octuplet machines
  sudo cobbler system add --name=manjula    --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a4:27:c2 --interface=eth0 --ip-address=10.10.0.100 --dns-name="manjula.escience.local"
  sudo cobbler system add --name=octuplet01 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a4:27:c2 --interface=eth0 --ip-address=10.10.0.101 --dns-name="octuplet01.escience.local"
  sudo cobbler system add --name=octuplet02 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a6:3e:bc --interface=eth0 --ip-address=10.10.0.102 --dns-name="octuplet02.escience.local"
  sudo cobbler system add --name=octuplet03 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a4:27:fe --interface=eth0 --ip-address=10.10.0.103 --dns-name="octuplet03.escience.local"
  sudo cobbler system add --name=octuplet04 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a4:28:80 --interface=eth0 --ip-address=10.10.0.104 --dns-name="octuplet04.escience.local"
  sudo cobbler system add --name=octuplet05 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a4:27:7a --interface=eth0 --ip-address=10.10.0.105 --dns-name="octuplet05.escience.local"
  sudo cobbler system add --name=octuplet06 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a4:27:82 --interface=eth0 --ip-address=10.10.0.106 --dns-name="octuplet06.escience.local"
  sudo cobbler system add --name=octuplet07 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a2:c7:58 --interface=eth0 --ip-address=10.10.0.107 --dns-name="octuplet07.escience.local"
  sudo cobbler system add --name=octuplet08 --profile=ubuntu-server-12.04.1-x86_64 --mac=00:25:90:a6:3e:96 --interface=eth0 --ip-address=10.10.0.108 --dns-name="octuplet08.escience.local"
  
One could also add these hosts to ethers, such to more easily grab access to the IPMI-interfaces::

  sudo vim /etc/cobbler/dnsmasq.template
  
  # Octuplet IPMI interfaces
  dhcp-host=00:25:90:a4:27:05,10.10.0.111
  dhcp-host=00:25:90:a6:3e:1c,10.10.0.112
  dhcp-host=00:25:90:a4:27:23,10.10.0.113
  dhcp-host=00:25:90:a4:27:64,10.10.0.114
  dhcp-host=00:25:90:a4:26:e1,10.10.0.115
  dhcp-host=00:25:90:a4:26:e5,10.10.0.116
  dhcp-host=00:25:90:a2:c6:7b,10.10.0.117
  dhcp-host=00:25:90:a6:3e:09,10.10.0.118

Removing Systems / Machines
---------------------------

Machines are simply removed by the command providing the system-name::

  sudo cobbler remove --name=<MACHINE_NAME>
  
Removing all we got::
  
  sudo cobbler system remove --name=octuplet01
  sudo cobbler system remove --name=octuplet02
  sudo cobbler system remove --name=octuplet03
  sudo cobbler system remove --name=octuplet04
  sudo cobbler system remove --name=octuplet05
  sudo cobbler system remove --name=octuplet06
  sudo cobbler system remove --name=octuplet07
  sudo cobbler system remove --name=octuplet08
  

Configuring Puppet
------------------

Setup puppetmaster::

  sudo vim /etc/puppet/puppet.conf
  
Important part is giving the master a name::

  [main]
  server=puppet
  
  [master]
  certname=puppet

Setup puppet-agent::

  sudo vim /etc/default/puppet

Enable::

  START=yes

The remaining options can be left at the defaults.
Next configure access to the puppet-fileserver::

  sudo vim /etc/puppet/fileserver.conf
  
By allowing access from the escience-lan::

  [files]
    path /etc/puppet/files
    allow *.escience.local
    allow 10.10.0.0/24  

Configuring Slurm
-----------------

Create the clurm configuration file::

  sudo vim /etc/slurm-llnl/slurm.conf
  
Add::

  # slurm.conf file generated by configurator.html.
  # Put this file on all nodes of your cluster.
  # See the slurm.conf man page for more information.
  #
  ControlMachine=skinner
  ControlAddr=10.10.0.1
  #BackupController=
  #BackupAddr=
  # 
  AuthType=auth/munge
  CacheGroups=0
  #CheckpointType=checkpoint/none 
  CryptoType=crypto/munge
  #DisableRootJobs=NO 
  #EnforcePartLimits=NO 
  #Epilog=
  #EpilogSlurmctld= 
  #FirstJobId=1 
  #MaxJobId=999999 
  #GresTypes= 
  #GroupUpdateForce=0 
  #GroupUpdateTime=600 
  #JobCheckpointDir=/var/slurm/checkpoint 
  #JobCredentialPrivateKey=
  #JobCredentialPublicCertificate=
  #JobFileAppend=0 
  #JobRequeue=1 
  #JobSubmitPlugins=1 
  #KillOnBadExit=0 
  #Licenses=foo*4,bar 
  #MailProg=/bin/mail 
  #MaxJobCount=5000 
  #MaxStepCount=40000 
  #MaxTasksPerNode=128 
  MpiDefault=none
  #MpiParams=ports=#-# 
  #PluginDir= 
  #PlugStackConfig= 
  #PrivateData=jobs 
  ProctrackType=proctrack/pgid
  #Prolog=
  #PrologSlurmctld= 
  #PropagatePrioProcess=0 
  #PropagateResourceLimits= 
  #PropagateResourceLimitsExcept= 
  ReturnToService=1
  #SallocDefaultCommand= 
  SlurmctldPidFile=/var/run/slurmctld.pid
  SlurmctldPort=6817
  SlurmdPidFile=/var/run/slurmd.pid
  SlurmdPort=6818
  SlurmdSpoolDir=/tmp/slurmd
  SlurmUser=slurm
  #SlurmdUser=root 
  #SrunEpilog=
  #SrunProlog=
  StateSaveLocation=/tmp
  SwitchType=switch/none
  #TaskEpilog=
  TaskPlugin=task/none
  #TaskPluginParam=
  #TaskProlog=
  #TopologyPlugin=topology/tree 
  #TmpFs=/tmp 
  #TrackWCKey=no 
  #TreeWidth= 
  #UnkillableStepProgram= 
  #UsePAM=0 
  # 
  # 
  # TIMERS 
  #BatchStartTimeout=10 
  #CompleteWait=0 
  #EpilogMsgTime=2000 
  #GetEnvTimeout=2 
  #HealthCheckInterval=0 
  #HealthCheckProgram= 
  InactiveLimit=0
  KillWait=30
  #MessageTimeout=10 
  #ResvOverRun=0 
  MinJobAge=300
  #OverTimeLimit=0 
  SlurmctldTimeout=120
  SlurmdTimeout=300
  #UnkillableStepTimeout=60 
  #VSizeFactor=0 
  Waittime=0
  # 
  # 
  # SCHEDULING 
  #DefMemPerCPU=0 
  FastSchedule=1
  #MaxMemPerCPU=0 
  #SchedulerRootFilter=1 
  #SchedulerTimeSlice=30 
  SchedulerType=sched/backfill
  SchedulerPort=7321
  SelectType=select/linear
  #SelectTypeParameters=
  # 
  # 
  # JOB PRIORITY 
  #PriorityType=priority/basic 
  #PriorityDecayHalfLife= 
  #PriorityCalcPeriod= 
  #PriorityFavorSmall= 
  #PriorityMaxAge= 
  #PriorityUsageResetPeriod= 
  #PriorityWeightAge= 
  #PriorityWeightFairshare= 
  #PriorityWeightJobSize= 
  #PriorityWeightPartition= 
  #PriorityWeightQOS= 
  # 
  # 
  # LOGGING AND ACCOUNTING 
  #AccountingStorageEnforce=0 
  #AccountingStorageHost=
  #AccountingStorageLoc=
  #AccountingStoragePass=
  #AccountingStoragePort=
  AccountingStorageType=accounting_storage/none
  #AccountingStorageUser=
  AccountingStoreJobComment=YES
  ClusterName=cluster
  #DebugFlags= 
  #JobCompHost=
  #JobCompLoc=
  #JobCompPass=
  #JobCompPort=
  JobCompType=jobcomp/none
  #JobCompUser=
  JobAcctGatherFrequency=30
  JobAcctGatherType=jobacct_gather/none
  SlurmctldDebug=3
  #SlurmctldLogFile=
  SlurmdDebug=3
  #SlurmdLogFile=
  #SlurmSchedLogFile= 
  #SlurmSchedLogLevel= 
  # 
  # 
  # POWER SAVE SUPPORT FOR IDLE NODES (optional) 
  #SuspendProgram= 
  #ResumeProgram= 
  #SuspendTimeout= 
  #ResumeTimeout= 
  #ResumeRate= 
  #SuspendExcNodes= 
  #SuspendExcParts= 
  #SuspendRate= 
  #SuspendTime= 
  # 
  # 
  # COMPUTE NODES 
  NodeName=octuplet0[1-8] CPUs=2 RealMemory=122880 Sockets=2 CoresPerSocket=16 ThreadsPerCore=1 State=UNKNOWN 
  PartitionName=octuplets Nodes=octuplet0[1-8] Default=YES MaxTime=INFINITE State=UP

Then restart slurm::

  sudo /usr/sbin/create-munge-key
  sudo service slurm restart
  sudo service munge start
  
This configuration is distributed to nodes via puppet.

Troubleshooting
---------------

Where to look when shit hits the fan::

  /etc/network/interfaces
  /etc/sysctl.conf
  /etc/cobbler/settings
  /etc/cobbler/modules.conf
  /etc/cobbler/dnsmasq.template
  /var/lib/cobbler/*

SSH-Tunnel to IPMI-WEB
----------------------

Enter::

  ssh -L 8111:10.10.0.111:80 \
      -L 8112:10.10.0.112:80 \
      -L 8113:10.10.0.113:80 \
      -L 8114:10.10.0.114:80 \
      -L 8115:10.10.0.115:80 \
      -L 8116:10.10.0.116:80 \
      -L 8117:10.10.0.117:80 \
      -L 8118:10.10.0.118:80 \
      -L 2201:10.10.0.101:22 \
      -L 2202:10.10.0.102:22 \
      -L 2203:10.10.0.103:22 \
      -L 2204:10.10.0.104:22 \
      -L 2205:10.10.0.105:22 \
      -L 2206:10.10.0.106:22 \
      -L 2207:10.10.0.107:22 \
      -L 2208:10.10.0.108:22 \
      escience@skinner.esci.nbi.dk

Puppet
======

On Puppetmaster / skinner
-------------------------

Remove all certs from puppet::

  sudo puppet cert clean octuplet01.escience.local
  sudo puppet cert clean octuplet02.escience.local
  sudo puppet cert clean octuplet03.escience.local
  sudo puppet cert clean octuplet04.escience.local
  sudo puppet cert clean octuplet05.escience.local
  sudo puppet cert clean octuplet06.escience.local
  sudo puppet cert clean octuplet07.escience.local
  sudo puppet cert clean octuplet08.escience.local
  true

List clients which are waiting to get their certificates signed::

  sudo puppetca list
  
Sign a certificate::

  sudo puppetca --sign octuplet01.escience.local
  
Or if you know what you are doing::

  sudo puppetcat --sign --all

On the clients
--------------

Remove current cert::

  rm -f /var/lib/puppet/ssl/certs/octuplet0*.escience.local.pem
  
Test connectivity::

  puppet agent --test --noop

SSH: Keybased login
-------------------

Make sure that users have public-keys in::

  sudo mkdir /home/username/.ssh
  sudo touch /home/username/.ssh/authorized_keys
  sudo chown -R username:username /home/username/.ssh
  sudo chmod 600 /home/username/.ssh/authorized_keys
  sudo vim /home/username/.ssh/authorized_keys

References
----------

The following sites was used in brewing up the above information::

  http://cobbler.github.com/manuals/
  https://help.ubuntu.com/community/Cobbler/Installation
  http://terrarum.net/administration/deploying-ubuntu-with-cobbler.html
  https://help.ubuntu.com/lts/installation-guide/amd64/preseed-contents.html
  https://help.ubuntu.com/12.04/installation-guide/example-preseed.txt
  http://ghantoos.org/2012/10/21/cocktail-of-pxe-debian-preseed-ipmi-puppet/
  https://help.ubuntu.com/community/SettingUpNFSHowTo
  https://computing.llnl.gov/linux/slurm/configurator.html
